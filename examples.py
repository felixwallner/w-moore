from numbers import Number

from aalpy.automata import MooreMachine, MooreState


def gen_moore_from_state_setup(state_setup) -> MooreMachine:
    # state_setup shoud map from state_id to tuple(output and transitions_dict)

    # build states with state_id and output
    states = {key: MooreState(key, val[0]) for key, val in state_setup.items()}

    # add transitions to states
    for state_id, state in states.items():
        for _input, target_state_id in state_setup[state_id][1].items():
            state.transitions[_input] = states[target_state_id]

    # states to list
    states = [state for state in states.values()]

    # build moore machine with first state as starting state
    mm = MooreMachine(states[0], states)

    # TODO: should we generate prefixes?
    # for state in states:
    #     state.prefix = mm.get_shortest_path(mm.initial_state, state)

    return mm


def gen_fully_connected(n: int = 100, d: int = 0, rseed: int = 42, loop_first: bool = False):
    from random import choice, seed

    # first = "x" if loop_first else "y"
    # second = "y" if loop_first else "x"

    r = n + d
    if rseed is not None:
        seed(rseed)
    return gen_moore_from_state_setup(
        {
            i: (
                i % n,
                {
                    "x": ((i + 1) % r) if i % 3 else choice(range(r)),
                    "y": choice(range(r)) if not i % 3 else ((i + 1) % r),
                },
            )
            for i in range(r)
        }
    )


def gen_fully_connected_2(states: int = 100, unique: int = 0):
    from random import choice

    assert 0 <= unique <= states, f"states:{states}, unique:{unique}"
    r = states
    n = states - unique

    return gen_moore_from_state_setup(
        {
            i: (
                i % n if n > 0 else i,
                {
                    "x": choice(range(r)),
                    "y": ((i + 1) % r),
                },
            )
            for i in range(r)
        }
    )


def gen_all_combinations(n: int = 4, d: int = 0, loop_first: bool = False):
    from itertools import combinations_with_replacement as cp

    first = "x" if loop_first else "y"
    second = "y" if loop_first else "x"

    r = n + d
    for c in cp(range(r), r):
        yield gen_moore_from_state_setup(
            {i: (min(i, n), {first: ((i + 1) % r), second: c[i]}) for i in range(r)}
        )


def gen_example_no_unique_1():
    """Very hard. All outputs have multiple states"""

    # map state_id to (output, transitions)
    state_setup = {
        "a1": ("a", {"x": "b1", "y": "a2"}),
        "b1": ("b", {"x": "c1", "y": "b2"}),
        "c1": ("c", {"x": "a1", "y": "c2"}),
        "a2": ("a", {"x": "c2", "y": "a1"}),
        "b2": ("b", {"x": "a2", "y": "b1"}),
        "c2": ("c", {"x": "b2", "y": "c1"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_1():
    """Original example. 2 OAS states with single distinguisher"""

    # map state_id to (output, transitions)
    state_setup = {
        "a": ("a", {"x": "b", "y": "a"}),
        "b": ("b", {"x": "dxyxf", "y": "c"}),
        "c": ("c", {"x": "dxyxa", "y": "a"}),
        "dxyxa": ("d", {"x": "exa", "y": "dxyxa"}),
        "dxyxf": ("d", {"x": "exf", "y": "dxyxf"}),
        "exa": ("e", {"x": "a", "y": "exa"}),
        "exf": ("e", {"x": "f", "y": "exf"}),
        "f": ("f", {"x": "a", "y": "f"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_2():
    """Very hard. All outputs have multiple states"""

    # map state_id to (output, transitions)
    state_setup = {
        "a1": ("a", {"x": "b1", "y": "a2"}),
        "b1": ("b", {"x": "c", "y": "b2"}),
        "a2": ("a", {"x": "c", "y": "a1"}),
        "b2": ("b", {"x": "a2", "y": "b1"}),
        "c": ("c", {"x": "b2", "y": "c"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_3():
    """A more complicated example where the first run checks with eq
    and navigation via trace is a must"""

    # map state_id to (output, transitions)
    state_setup = {
        "a": ("a", {"x": "b1", "y": "b2"}),
        "b1": ("b", {"x": "c1", "y": "a"}),
        "b2": ("b", {"x": "c2", "y": "b3"}),
        "b3": ("b", {"x": "c1", "y": "b2"}),
        "c1": ("c", {"x": "d", "y": "a"}),
        "c2": ("c", {"x": "e", "y": "a"}),
        "d": ("d", {"x": "a", "y": "a"}),
        "e": ("e", {"x": "a", "y": "a"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_4():
    """Similar to example 3 but a bit easier."""

    # map state_id to (output, transitions)
    state_setup = {
        "a": ("a", {"x": "b1", "y": "b2"}),
        "b1": ("b", {"x": "c1", "y": "a"}),
        "b2": ("b", {"x": "c2", "y": "b1"}),
        "c1": ("c", {"x": "d", "y": "a"}),
        "c2": ("c", {"x": "e", "y": "a"}),
        "d": ("d", {"x": "a", "y": "a"}),
        "e": ("e", {"x": "a", "y": "a"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_5():
    """An automaton which is not fully connected
    and where abandoning states is required"""

    # map state_id to (output, transitions)
    state_setup = {
        "nfc": ("nfc", {"x": "a", "y": "a"}),
        "a": ("a", {"x": "b1", "y": "b2"}),
        "b1": ("b", {"x": "c", "y": "a"}),
        "b2": ("b", {"x": "a", "y": "d"}),
        "c": ("c", {"x": "a", "y": "a"}),
        "d": ("d", {"x": "a", "y": "a"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_6():
    """An automaton where the initial state's output
    appears twice. Requires setting of initial state for hypothesis"""

    # map state_id to (output, transitions)
    state_setup = {
        "a1": ("a", {"x": "a1", "y": "a2"}),
        "a2": ("a", {"x": "a1", "y": "b"}),
        "b": ("b", {"x": "b", "y": "a2"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_7():
    """Harder: An automaton where the initial state's output
    appears multiple times. Requires setting of initial state for hypothesis"""

    # map state_id to (output, transitions)
    state_setup = {
        "a1": ("a", {"x": "a3", "y": "a2"}),
        "a2": ("a", {"x": "a4", "y": "a3"}),
        "a3": ("a", {"x": "b", "y": "a4"}),
        "a4": ("a", {"x": "b", "y": "a1"}),
        "b": ("b", {"x": "b", "y": "a1"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_8():
    """Harder: 3 inputs and multiple distinguishers"""

    # map state_id to (output, transitions)
    state_setup = {
        "a": ("a", {"x": "b1", "y": "b2", "z": "b3"}),
        "b1": ("b", {"x": "c1", "y": "c2", "z": "c1"}),
        "b2": ("b", {"x": "c1", "y": "c1", "z": "c3"}),
        "b3": ("b", {"x": "c2", "y": "c2", "z": "c3"}),
        "c1": ("c", {"x": "d", "y": "a", "z": "a"}),
        "c2": ("c", {"x": "a", "y": "d", "z": "a"}),
        "c3": ("c", {"x": "a", "y": "a", "z": "d"}),
        "d": ("d", {"x": "a", "y": "a", "z": "a"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_9():
    """Harder: 3 inputs and multiple distinguishers.
    Like 8 but shorter"""

    # map state_id to (output, transitions)
    state_setup = {
        "a": ("a", {"x": "b1", "y": "b2", "z": "b3"}),
        "b1": ("b", {"x": "c", "y": "a", "z": "a"}),
        "b2": ("b", {"x": "a", "y": "c", "z": "a"}),
        "b3": ("b", {"x": "a", "y": "a", "z": "c"}),
        "c": ("c", {"x": "a", "y": "a", "z": "a"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_10():
    """Another hard version"""

    # map state_id to (output, transitions)
    state_setup = {
        "0": ("0", {"x": "4", "y": "2"}),
        "1": ("1", {"x": "0", "y": "1"}),
        "2": ("2", {"x": "0", "y": "2"}),
        "3": ("3", {"x": "0", "y": "3"}),
        "4": ("1", {"x": "0", "y": "4"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_11():
    """A state with an non-strongly connected start state whose output os duplicated"""

    # map state_id to (output, transitions)
    state_setup = {
        "nfc_init": ("nfc", {"x": "a", "y": "a"}),
        "a": ("a", {"x": "b", "y": "nfc_fc"}),
        "b": ("b", {"x": "c", "y": "a"}),
        "c": ("c", {"x": "a", "y": "a"}),
        "nfc_fc": ("nfc", {"x": "c", "y": "c"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_12():
    """A state with an non-strongly connected start state whose output os duplicated"""

    # map state_id to (output, transitions)
    state_setup = {
        "a": ("a", {"x": "b1", "y": "a"}),
        "b1": ("b", {"x": "c", "y": "b3"}),
        "c": ("c", {"x": "b2", "y": "c"}),
        "b2": ("b", {"x": "d", "y": "b1"}),
        "d": ("d", {"x": "b3", "y": "d"}),
        "b3": ("b", {"x": "a", "y": "b2"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_example_13():
    """A state with an non-strongly connected start state whose output os duplicated"""

    # map state_id to (output, transitions)
    state_setup = {
        "a": ("a", {"x": "b1", "y": "a"}),
        "b1": ("b", {"x": "b2", "y": "a"}),
        "b2": ("b", {"x": "b3", "y": "a"}),
        "b3": ("b", {"x": "b4", "y": "a"}),
        "b4": ("b", {"x": "b5", "y": "a"}),
        "b5": ("b", {"x": "b6", "y": "a"}),
        "b6": ("b", {"x": "b7", "y": "a"}),
        "b7": ("b", {"x": "c", "y": "a"}),
        "c": ("c", {"x": "a", "y": "a"}),
    }

    return gen_moore_from_state_setup(state_setup)


def gen_sm_1():
    """Partial Smoke Meter with waiting transitions"""

    # alphabet:
    # {"WAIT","SASB","SBSZ","SFPF","SKOR","SLEC","SMES","SMKA","SMRE","SPUL","SRDY","SVOP" }

    # map state_id to (output, transitions)
    state_setup = {
        "1": (
            "F 1 0 0",
            {
                "WAIT": "1",
                "SASB": "1",
                "SBSZ": "1",
                "SFPF": "1",
                "SKOR": "1",
                "SLEC": "5-1",
                "SMES": "1",
                "SMKA": "5-1",
                "SMRE": "7-1",
                "SPUL": "1",
                "SRDY": "2-1",
                "SVOP": "6-1",
            },
        ),
        "3": (
            "F 3 0 0",
            {
                "WAIT": "3",
                "SASB": "9",
                "SBSZ": "3",
                "SFPF": "13",
                "SKOR": "3",
                "SLEC": "5-2",
                "SMES": "8",
                "SMKA": "5-2",
                "SMRE": "7-2",
                "SPUL": "4",
                "SRDY": "2-1",
                "SVOP": "6-2",
            },
        ),
        "2-1": ("F 2 0 0", {"WAIT": "3"}),
        "2-2": ("F 2 0 0", {"WAIT": "1"}),
        "4": ("F 4 0 0", {"WAIT": "3"}),
        "5-1": ("F 5 0 0", {"WAIT": "1"}),
        "5-2": ("F 5 0 0", {"WAIT": "3"}),
        "6-1": ("F 6 0 0", {"WAIT": "1"}),
        "6-2": ("F 6 0 0", {"WAIT": "3"}),
        "7-1": ("F 7 0 0", {"WAIT": "1"}),
        "7-2": ("F 7 0 0", {"WAIT": "3"}),
        "8": ("F 8 0 0", {"WAIT": "4"}),
        "9": ("F 9 0 0", {"WAIT": "10"}),
        "10": ("F 10 0 0", {"WAIT": "2-2"}),
        "13": ("F 13 0 0", {"WAIT": "3"}),
    }

    return gen_moore_from_state_setup(state_setup)


def test_time_extension(moore_machine: MooreMachine, time_input: str):
    import copy
    from functools import partial

    from aalpy.SULs import MooreSUL

    import extension.ignore_on_time_triggered as IgnoreOnTT
    from util import get_alphabet
    from wmoore import NonResettingMooreSUL, run_wmoore

    assert not moore_machine.is_input_complete()

    IgnoreOnTT.load(time_input)
    alphabet = get_alphabet(moore_machine)
    alphabet.sort(key=time_input.__ne__)
    print(alphabet)
    assert time_input in alphabet
    assert time_input == alphabet[0]

    moore_machine_test = copy.deepcopy(moore_machine)

    sul = NonResettingMooreSUL(moore_machine)
    oracle_gen = partial(
        IgnoreOnTT.IgnoreOnTimeTriggeredRandomWalkOracle, alphabet, num_steps=1000
    )

    learned, info = run_wmoore(
        alphabet,
        sul,
        oracle_gen,
        10,
        return_data=True,
    )
    assert learned is not None and info is not None

    # finished learning, now test if correctly learned

    assert len(learned.states) <= len(
        moore_machine_test.states
    ), f"Learned model has too many states ({len(learned.states)} to {len(moore_machine_test.states)})"

    output = moore_machine_test.initial_state.output
    # assert (
    #     output == learned.initial_state.output
    # ), f"Initial state outputs differ: {output} vs learned {learned.initial_state.output}"
    if output != learned.initial_state.output:
        print(
            f"W: Initial state outputs differ: {output} vs learned {learned.initial_state.output}"
        )

    for pot_starting_state in filter(lambda s: s.output == output, moore_machine_test.states):
        moore_machine_test.initial_state = pot_starting_state
        sul = MooreSUL(moore_machine_test)
        assert_eq_oracle = oracle_gen(sul)
        if assert_eq_oracle.find_cex(learned) is None:
            break
    else:
        assert False, "All states with output result in counter example"


def run_comparison_and_check(
    moore_machine: MooreMachine, max_len: int, file=None, lstar=True, timeout=10
) -> None:
    import copy
    import sys
    import time
    from concurrent.futures import TimeoutError
    from functools import partial
    from pprint import pprint

    from aalpy.learning_algs import run_Lstar
    from aalpy.SULs import MooreSUL
    from pebble import concurrent

    from wmoore import NonResettingMooreSUL, PerfectEqOracle, run_wmoore

    if file is None:
        file = sys.stdout

    def check(learned: MooreMachine, moore_test: MooreMachine):
        assert not isinstance(learned, tuple)
        assert len(learned.states) <= len(moore_test.states), "Hypothesis has to many states!"

        output = learned.initial_state.output
        if moore_test.initial_state.output != output:
            print(
                f"W: Initial state outputs differ: {moore_test.initial_state.output} vs learned {output}"
            )

        for pot_starting_state in filter(lambda s: s.output == output, moore_test.states):
            moore_test.initial_state = pot_starting_state
            moore_test.reset_to_initial()
            sul = MooreSUL(moore_test)
            assert_eq_oracle = PerfectEqOracle(alphabet, sul, asserts=False)
            if assert_eq_oracle.find_cex(learned) is None:
                break
        else:
            assert False, "All states with output result in counter example"

    @concurrent.process(timeout=timeout)
    def run_wmoore_concurrent(alphabet, moore_machine, max_len):
        return run_wmoore(
            alphabet,
            NonResettingMooreSUL(moore_machine),
            partial(PerfectEqOracle, alphabet),
            max_len,
            return_data=True,
            visualize_with_name=None,
            print_level=0,
        )

    moore_cpy = copy.deepcopy(moore_machine)
    alphabet = sorted(moore_machine.get_input_alphabet())
    start1 = time.perf_counter()
    try:
        future = run_wmoore_concurrent(alphabet, moore_machine, max_len)
        learned, info1 = future.result()
        info1["time"] = time.perf_counter() - start1
        check(learned, copy.deepcopy(moore_cpy))
    except TimeoutError as e:
        # print(e, e.args)
        info1 = {
            "steps_learning": "BREAK",
            "queries_eq_oracle": "BREAK",
            "learning_rounds": "BREAK",
        }
    except RuntimeError as e:
        earg = e.args[0].split()
        if len(earg) == 6 and earg[:2] == ["No", "change"]:
            info1 = {
                "steps_learning": "NOCHANGE",
                "queries_eq_oracle": "NOCHANGE",
                "learning_rounds": int(earg[4]),
            }
        elif earg == ["DEBUG:", "Trace", "was", "too", "long"]:
            info1 = {
                "steps_learning": "LONG",
                "queries_eq_oracle": "LONG",
                "learning_rounds": "LONG",
            }
        elif earg[:2] == ["Known", "limitation:"]:
            info1 = {
                "steps_learning": "LIMIT",
                "queries_eq_oracle": "LIMIT",
                "learning_rounds": "LIMIT",
            }
        else:
            assert False, f"Unexpected runtime error: {e}, {e.args}"
    except KeyboardInterrupt:
        info1 = {
            "steps_learning": "BREAK",
            "queries_eq_oracle": "BREAK",
            "learning_rounds": "BREAK",
        }
    except AssertionError as e:
        info1 = {
            "steps_learning": "ASSERT",
            "queries_eq_oracle": "ASSERT",
            "learning_rounds": "ASSERT",
        }
        # print(e, e.args)
        # input("Assert?")
    if "time" not in info1.keys():
        info1["time"] = time.perf_counter() - start1

    if lstar:
        sul = MooreSUL(moore_machine)
        start2 = time.perf_counter()
        learned, info2 = run_Lstar(
            alphabet,
            sul,
            PerfectEqOracle(alphabet, sul, asserts=False),
            automaton_type="moore",
            return_data=True,
            print_level=0,
        )
        info2["time"] = time.perf_counter() - start2
        check(learned, copy.deepcopy(moore_cpy))
    else:
        info2 = None
    # print("Value: wmoore vs Lstar")
    # for key in info1.keys() & info2.keys():
    #     if key in ["steps_learning", "queries_eq_oracle", "learning_rounds"]:
    #         print(f"{key}: {info1[key]} vs {info2[key]}", file=file)
    return info1, info2


if __name__ == "__main__":
    import random
    import sys
    import time
    from statistics import mean, median, stdev

    from aalpy.utils.AutomatonGenerators import generate_random_moore_machine
    from tqdm import tqdm

    # overwrite repr in AALpy MooreState for easier visualisation
    # MooreState.__repr__ = lambda s: f"MS({s.output})"
    import util

    def run_n(n, states, unique, max_len, f, lstar=True, timeout=10, use_fc=False):
        from string import ascii_lowercase

        print(n, states, unique)
        input_alphabet = ["x", "y"]
        output_alphabet = list(ascii_lowercase[:states])
        # print(output_alphabet)
        # with open(
        #     f"ntests_{states}_u{unique}_{input_alphabet}_{output_alphabet}_{int(time.time())}.log",
        #     "w",
        # ) as f:
        steps = [], []
        eq = [], []
        rounds = [], []
        timings = [], []
        for i in tqdm(range(0, n), desc="Iteration"):
            # print(f"Iteration {i}", file=f)
            # print(f"Iteration {i}")
            for tries in range(100000):
                # print(tries)
                random.seed(f"seed: {states}-{unique}-{n}-{i}-{tries}")
                if use_fc:
                    machine = gen_fully_connected_2(states, unique)
                else:
                    machine = generate_random_moore_machine(
                        states, input_alphabet, output_alphabet
                    )
                assert len(machine.states) == states
                m_unique = util.unique_outputs(machine)
                # print(m_unique)
                # print(machine.is_strongly_connected())
                if (
                    (unique is None and m_unique > 0)
                    or (unique is not None and m_unique == unique)
                ) and machine.is_strongly_connected():
                    break
            else:
                # assert False, "Did not find a configuration with unique outputs"
                # print("Did not find a configuration with unique outputs")
                break
            # print(f"Unique outputs: {util.unique_outputs(machine)}", file=f)
            # if i == 160:
            #     input("Waiting...")
            info1, info2 = run_comparison_and_check(
                machine, max_len, file=sys.stdout, timeout=timeout
            )
            steps[0].append(info1["steps_learning"])
            steps[1].append(info2.get("steps_learning"))
            eq[0].append(info1["queries_eq_oracle"])
            eq[1].append(info2.get("queries_eq_oracle"))
            rounds[0].append(info1["learning_rounds"])
            rounds[1].append(info2.get("learning_rounds"))
            timings[0].append(info1["time"])
            timings[1].append(info2.get("time"))
        else:
            print(
                ",".join(
                    map(
                        str,
                        [
                            "wmoore",
                            states,
                            unique,
                            max_len,
                            n,
                            sum(1 for i in steps[0] if i == "NOCHANGE"),
                            sum(1 for i in steps[0] if i == "LONG"),
                            sum(1 for i in steps[0] if i == "LIMIT"),
                            sum(1 for i in steps[0] if i == "BREAK"),
                            sum(1 for i in steps[0] if i == "ASSERT"),
                            sum(1 for i in steps[0] if isinstance(i, Number)),
                            mean(filter(lambda v: isinstance(v, Number), steps[0])),
                            median(filter(lambda v: isinstance(v, Number), steps[0])),
                            stdev(filter(lambda v: isinstance(v, Number), steps[0])),
                            mean(filter(lambda v: isinstance(v, Number), eq[0])),
                            median(filter(lambda v: isinstance(v, Number), eq[0])),
                            stdev(filter(lambda v: isinstance(v, Number), eq[0])),
                            mean(filter(lambda v: isinstance(v, Number), rounds[0])),
                            median(filter(lambda v: isinstance(v, Number), rounds[0])),
                            stdev(filter(lambda v: isinstance(v, Number), rounds[0])),
                            mean(filter(lambda v: isinstance(v, Number), timings[0])),
                            median(filter(lambda v: isinstance(v, Number), timings[0])),
                            stdev(filter(lambda v: isinstance(v, Number), timings[0])),
                        ],
                    )
                ),
                file=f,
            )
            if lstar:
                print(
                    ",".join(
                        map(
                            str,
                            [
                                "L*",
                                states,
                                unique,
                                max_len,
                                n,
                                sum(1 for i in steps[1] if i == "NOCHANGE"),
                                sum(1 for i in steps[1] if i == "LONG"),
                                sum(1 for i in steps[1] if i == "LIMIT"),
                                sum(1 for i in steps[0] if i == "BREAK"),
                                sum(1 for i in steps[0] if i == "ASSERT"),
                                sum(1 for i in steps[1] if isinstance(i, Number)),
                                mean(filter(lambda v: isinstance(v, Number), steps[1])),
                                median(filter(lambda v: isinstance(v, Number), steps[1])),
                                stdev(filter(lambda v: isinstance(v, Number), steps[1])),
                                mean(filter(lambda v: isinstance(v, Number), eq[1])),
                                median(filter(lambda v: isinstance(v, Number), eq[1])),
                                stdev(filter(lambda v: isinstance(v, Number), eq[1])),
                                mean(filter(lambda v: isinstance(v, Number), rounds[1])),
                                median(filter(lambda v: isinstance(v, Number), rounds[1])),
                                stdev(filter(lambda v: isinstance(v, Number), rounds[1])),
                                mean(filter(lambda v: isinstance(v, Number), timings[1])),
                                median(filter(lambda v: isinstance(v, Number), timings[1])),
                                stdev(filter(lambda v: isinstance(v, Number), timings[1])),
                            ],
                        )
                    ),
                    file=f,
                )

    n = 1000
    with open(f"ntests_n{n}_{int(time.time())}.csv", "w") as f:
        print(
            ",".join(
                [
                    "ALGO",
                    "states",
                    "unique",
                    "max_len",
                    "iterations n",
                    "NOCHANGE",
                    "LONG",
                    "LIMIT",
                    "BREAK",
                    "ASSERT",
                    "REST",
                    "MEAN_STEPS",
                    "MED_STEPS",
                    "STD_STEPS",
                    "MEAN_EQ",
                    "MED_EQ",
                    "STD_EQ",
                    "MEAN_ROUNDS",
                    "MED_ROUNDS",
                    "STD_ROUNDS",
                    "MEAN_TIME",
                    "MED_TIME",
                    "STD_TIME",
                ]
            ),
            file=f,
        )

        def table_a_1():
            """produce table with constant 8 staes but different max_len"""
            states = 8
            for max_len in tqdm(range(0, 16), desc="max_len"):
                for unique in tqdm(range(2, states + 1), desc="unique"):
                    run_n(n, states, unique, max_len, f, False)

        def table_a_2():
            """produce table with same amount of states and max_len"""
            for states in tqdm(range(3, 15), desc="states"):
                for unique in tqdm(range(2, states + 1), desc="unique"):
                    run_n(n, states, unique, states, f)

        def table_a_3_and_a_4():
            """produce table for L* and results with constant max_len = 10"""
            for states in tqdm(range(3, 15), desc="states"):
                for unique in tqdm(range(2, states + 1), desc="unique"):
                    run_n(n, states, unique, 10, f)

        def other_generation():
            """wanted to try with big examples but generating via use_fc=True is probabily biased"""
            states = 50
            for max_len in tqdm(range(0, 100, 25), desc="max_len"):
                for unique in tqdm(range(states, states - 5, -1), desc="unique"):
                    run_n(n, states, unique, max_len, f, False, timeout=10, use_fc=True)

        table_a_1()
        # table_a_2()
        # table_a_3_and_a_4()
