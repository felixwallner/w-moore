# W-Moore Algorithm

Reset-less active automata learning algorithm for Moore machines developed in Python with the [AALpy](https://github.com/DES-Lab/AALpy) framework.
It was developed as part of the [LearnTwins](https://learntwins.ist.tugraz.at/about/) project at [TUGraz](https://www.tugraz.at/institute/ist/home/) in collaboration with [AIT](https://www.ait.ac.at/) and [AVL](https://www.avl.com) during a master thesis.


### Notes

Last tested with AALpy version 1.2.3,
compatible with Python3.8+

### Maintainer

Felix Wallner <felix.wallner@ist.tugraz.at>

### License

MIT
