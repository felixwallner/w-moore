import random
from typing import List

from aalpy.base import SUL, Automaton, AutomatonState, Oracle

import wmoore


class TimingAwareRandomWalkEqOracle(Oracle):
    def __init__(
        self,
        waiting_input: wmoore.Input,
        alphabet: List[wmoore.Input],
        sul: SUL,
        num_steps: int = 500,
        reset_after_cex: bool = True,
        reset_prob: float = -1,
        max_steps_in_timed: int = 2,
    ):
        """
        Args:
            waiting_input: input that indicates timed transitions,
                will prefer this input if the state has non-self transition
                and the max_steps_in_timed was reached in the current state

            alphabet: input alphabet

            sul: system under learning

            num_steps: number of steps to be preformed

            reset_after_cex: if true, num_steps will be preformed after every counter example,
                else the total number or steps will equal to num_steps

            reset_prob: probability that the new query will be asked (Default: -1 => never reset)

            max_steps_in_timed: the max number of steps with self transition
                that may be performed in a timed state
        """
        super().__init__(alphabet, sul)
        self.step_limit = num_steps
        self.reset_after_cex = reset_after_cex
        self.reset_prob = reset_prob
        self.waiting_input = waiting_input
        self.max_steps_in_timed = max_steps_in_timed
        self.random_steps_done = 0
        assert (
            waiting_input in self.alphabet
        ), f"Waiting input '{waiting_input}' is expected to be in alphabet {alphabet}"
        assert (
            waiting_input == self.alphabet[0]
        ), "Waiting input should always be first in alphabet"

    def find_cex(self, hypothesis: Automaton):
        inputs = []
        steps_in_timed: int = 0
        last_state: AutomatonState = None
        self.reset_hyp_and_sul(hypothesis)

        while self.random_steps_done < self.step_limit:
            self.num_steps += 1
            self.random_steps_done += 1

            if random.random() <= self.reset_prob:
                self.reset_hyp_and_sul(hypothesis)
                inputs.clear()
                steps_in_timed = 0
                last_state = None

            current_state: AutomatonState = hypothesis.current_state
            if last_state is not current_state:
                steps_in_timed = 0
                last_state = current_state
            timed_target: AutomatonState = current_state.transitions[self.waiting_input]
            timed = current_state is not timed_target
            steps_in_timed += 1 if timed else 0

            if timed and steps_in_timed > self.max_steps_in_timed:
                inputs.append(self.waiting_input)
            else:
                inputs.append(random.choice(self.alphabet))

            out_sul = self.sul.step(inputs[-1])
            out_hyp = hypothesis.step(inputs[-1])

            if out_sul != out_hyp:
                if self.reset_after_cex:
                    self.random_steps_done = 0
                self.sul.post()
                return inputs

        return None
