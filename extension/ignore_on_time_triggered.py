from functools import partial, partialmethod
from typing import List, Optional

from aalpy.automata import MooreMachine, MooreState
from aalpy.base import SUL

import wmoore

from .timing_aware_oracle import TimingAwareRandomWalkEqOracle as _TimingAwareRandomWalkEqOracle


def get_timing_input() -> wmoore.Input:
    raise NotImplementedError("Should be overwritten via module's load function")


def is_state_timed(state: wmoore.NonDeterministicMooreState) -> bool:
    timing_input = get_timing_input()
    if timing_input not in state.transitions:
        return False
    timed_targets: List[wmoore.NonDeterministicMooreState] = state.transitions[timing_input]
    timed = any(state is not target for target in timed_targets)
    return timed


class IgnoreOnTimeTriggeredNonDeterministicHypothesis(wmoore.NonDeterministicHypothesis):
    def is_transition_missing(
        self, state: Optional[wmoore.NonDeterministicMooreState] = None
    ) -> bool:
        """Checks if a certain state - or any state if state is None - is missing a transition"""
        if state is None:
            return any(
                self.is_transition_missing(state) for state in self.states if not state.abandoned
            )
        else:
            if is_state_timed(state):
                # if we know this state is timed, then no other transitions should be checked
                # ie. no other transitions are missing
                return False
            return super().is_transition_missing(state)

    def provide_missing_transition(
        self, state: wmoore.NonDeterministicMooreState
    ) -> Optional[wmoore.Input]:
        if is_state_timed(state):
            timing_input = get_timing_input()
            if (
                timing_input not in state.transitions.keys()
                or len(state.transitions[timing_input]) == 0
            ):
                return timing_input
            return None
        return super().provide_missing_transition(state)

    def gen_det_hypothesis(self, starting_state=None):
        assert starting_state is None or starting_state in self.states
        assert starting_state is None or not starting_state.abandoned
        # abandoned_sink = MooreState(-1, "abandoned-sink")
        states = [
            MooreState(nd_state.state_id, nd_state.output)
            for nd_state in self.states
            if not nd_state.abandoned
        ]
        nd_states = [s for s in self.states if not s.abandoned]
        assert len(nd_states) == len(states)

        initial_state = (
            states[0]
            if starting_state is None
            else next(state for state in states if state.state_id == starting_state.state_id)
        )

        # set all transitions
        for nd_state, state in zip(nd_states, states):
            assert nd_state.state_id == state.state_id
            assert nd_state.output == state.output
            for letter, nd_next_states in nd_state.transitions.items():
                if not nd_next_states:
                    assert letter != get_timing_input()
                    continue  # should only be required in partial hypothesis (timed extension)

                assert not all(
                    s.abandoned for s in nd_next_states
                ), f"ALL state transitions abanoned from {nd_state} with {letter} to {nd_next_states}"
                next_nd_state = next(s for s in nd_next_states if not s.abandoned)
                assert all(
                    s.abandoned for s in nd_next_states if s is not next_nd_state
                ), "All other states SHOULD be abanoned"
                next_state = next(s for s in states if s.state_id == next_nd_state.state_id)
                state.transitions[letter] = next_state

        # create automaton and characterization set
        automaton = MooreMachine(initial_state, states)
        automaton.characterization_set = [tuple([a]) for a in self.alphabet]
        # empty word for moore
        automaton.characterization_set.insert(0, tuple())

        # calculate prefixes
        for state in states:
            state.prefix = automaton.get_shortest_path(automaton.initial_state, state)

        return automaton


class IgnoreOnTimeTriggeredRandomWalkOracle(_TimingAwareRandomWalkEqOracle):
    def __init__(
        self,
        alphabet: List[wmoore.Input],
        sul: SUL,
        num_steps: int = 500,
        reset_after_cex: bool = True,
        reset_prob: float = -1,
    ):
        """
        Call modules 'load' function to correctly setup this class before initializing it!

        Args:
            alphabet: input alphabet

            sul: system under learning

            num_steps: number of steps to be preformed

            reset_after_cex: if true, num_steps will be preformed after every counter example, else the total number
                or steps will equal to num_steps

            reset_prob: probability that the new query will be asked (Default: -1 => never reset)
        """
        raise NotImplementedError("Should be overwritten via module's load function")


def leave_timed(ndsul: wmoore.NonDeterministicSulWrapper):
    if is_state_timed(ndsul.hypothesis.current_state):
        reached = wmoore.navigate_to_nearest(
            ndsul.step_expected,
            lambda state: not is_state_timed(state),
            ndsul.hypothesis,
            ndsul.trace,
        )
        print(f"I: Hook: Leaving timed! Reached: {reached}")


def load(timing_input: wmoore.Input):
    """Overwrites the required parts of the algorithm to ignore other transitions in time-triggered states:
    Overwrites:
        * wmoore.NonDeterminsticHypothesis
        * IgnoreOnTimeTriggeredRandomWalkOracle"""
    print("W: Activating extension! Global scope classes and functions may be overwritten!")
    global get_timing_input

    def _get_timing_input():
        return timing_input

    get_timing_input = _get_timing_input
    IgnoreOnTimeTriggeredRandomWalkOracle.__init__ = partialmethod(
        _TimingAwareRandomWalkEqOracle.__init__, timing_input, max_steps_in_timed=0
    )
    wmoore.NonDeterministicHypothesis = IgnoreOnTimeTriggeredNonDeterministicHypothesis
    wmoore.after_1a_hook = leave_timed
