import time
from typing import Any, Dict, List, Tuple

from aalpy.base import SUL

import wmoore

TIMING_INFO_KEY = "step_timing"


class TimeStepsSulWrapper(wmoore.NonDeterministicSulWrapper):
    def __init__(
        self, sul: SUL, hypothesis: wmoore.NonDeterministicHypothesis, trace: wmoore.SingleTrace
    ) -> None:
        super().__init__(sul, hypothesis, trace)
        self._times: List[float] = []

    def add_info(self, info: Dict[str, Any]):
        super().add_info(info)
        timings = self._aggregate_times()
        info[TIMING_INFO_KEY] = timings

    def step(self, letter: wmoore.Input) -> wmoore.Output:
        now = time.perf_counter()
        out = self.sul.step(letter)
        delta = time.perf_counter() - now
        if letter is not None:
            self.update_hypothesis_and_trace(letter, out)
            self._times.append(delta)
            if self._raise_on_expected and not self._expected:
                self._raise_on_expected = False
                raise wmoore.NonDeterminismFound
        return out

    def _aggregate_times(self):
        assert len(self._times) == len(self.trace.trace)
        assert self.hypothesis.is_deterministic()
        results: Dict[Tuple[int, wmoore.Input], List[float]] = dict()
        last_states, src_states = [], []
        for t, delta in zip(self.trace.trace, self._times):
            src_out, with_letter, dest_out = t[0:3]
            if last_states:
                src_states = list(filter(lambda s: s.output == src_out, last_states))
                src_states = list(
                    filter(
                        lambda s: s.transitions.get(with_letter)
                        and s.transitions[with_letter][0].output == dest_out,
                        src_states,
                    )
                )
            if not src_states:
                src_states = self.hypothesis.states_with_output(src_out)
                src_states = list(
                    filter(
                        lambda s: s.transitions.get(with_letter)
                        and s.transitions[with_letter][0].output == dest_out,
                        src_states,
                    )
                )
            assert src_states, "No source_states found"
            if len(src_states) > 1:
                print(f"W: {len(src_states)} possible source states found")
            last_states = [
                s.transitions[with_letter][0]
                for s in src_states
                if s.transitions.get(with_letter)
                and s.transitions[with_letter][0].output == dest_out
            ]
            key = (src_states[0].state_id, with_letter)
            if key not in results:
                results[key] = []
            results[key].append(delta)
        return results


def load():
    """Overwrites the required parts of the algorithm to time all steps according to their state and input:
    Overwrites:
        * wmoore.NonDeterministicSulWrapper"""
    print("W: Activating extension! Global scope classes and functions may be overwritten!")
    wmoore.NonDeterministicSulWrapper = TimeStepsSulWrapper
