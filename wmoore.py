from __future__ import annotations  # requires python 3.7+

import copy
import itertools as it
import time
from functools import partial
from typing import Any, Callable, Dict, Generator, List, Literal, Optional, Set, Tuple, Union

from aalpy.automata import MooreMachine, MooreState
from aalpy.base import SUL, Automaton, Oracle
from aalpy.utils import save_automaton_to_file, visualize_automaton

visualize_path = "output/"

# hooks for extensions
after_1a_hook: Optional[Callable[[NonDeterministicSulWrapper], None]] = None


# trick mypy into correct typechecking, see:
# https://stackoverflow.com/questions/59360567/define-a-custom-type-that-behaves-like-typing-any
if False:
    Input = Any
    Output = Any
else:

    class Input:  # type: ignore
        """A single input ie. letter"""

        pass

    class Output:  # type: ignore
        pass


Word = Tuple[Input, ...]


class NonDeterminismFound(Exception):
    pass


class PerfectEqOracle(Oracle):
    def __init__(self, alphabet: list, sul: SUL, asserts: bool = True):
        super().__init__(alphabet, sul)
        self.asserts = asserts

    def find_cex(self, hypothesis):
        """
        Return a counterexample (inputs) that displays different behavior on system under learning and
        current hypothesis.

        Args:

          hypothesis: current hypothesis

        Returns:

            tuple or list containing counterexample inputs, None if no counterexample is found
        """
        self.reset_hyp_and_sul(hypothesis)
        sul = self.sul
        while hasattr(sul, "sul"):
            sul = sul.sul
        assert hasattr(sul, "mm"), f"MooreMachine should be in sul: {sul} ({type(sul)})"
        auto: MooreMachine = copy.deepcopy(sul.mm)
        assert (
            auto.get_input_alphabet() == self.alphabet
        ), f"{auto.get_input_alphabet()} != {self.alphabet}"
        try:
            dis = auto.find_distinguishing_seq(auto.current_state, hypothesis.current_state)
            assert dis, "Should raise exception if not dis found"
            # print("Found cex:", dis)
            if not self.asserts:
                return dis
            for inp in dis:
                out1 = self.sul.step(inp)
                out2 = hypothesis.step(inp)
                if out1 != out2:
                    # print("Found difference in output without exception??")
                    assert False, "Found difference in output without exception??"
            assert False, "Did not encounter difference in distinguishing sequence"
            return dis
        except SystemExit:
            # AALpy exists this function with SystemExit (which does not make sense), so we catch it
            # print("No counterexample found! No distinguishing sequence")
            return None


class NonResettingMooreSUL(SUL):
    """
    System under learning for non-resetting moore machines.
    """

    def __init__(self, moore_machine: MooreMachine) -> None:
        super().__init__()
        self.mm = moore_machine

    def pre(self) -> None:
        pass

    def post(self) -> None:
        pass

    def step(self, letter: Input) -> Output:
        if letter is None:
            return self.mm.current_state.output
        self.num_steps += 1
        # # Inject sporadic non-determinism as a test
        # if self.num_steps == 20:
        #     print("!!!: Using random transitions!")
        #     self.mm.current_state = list(
        #         filter(
        #             lambda s: s is not self.mm.current_state.transitions[letter], self.mm.states
        #         )
        #     )[0]
        return self.mm.step(letter)


class NonDeterministicMooreState:
    def __init__(self, state_id: int, output: Output) -> None:
        self.state_id: int = state_id
        self.output: Output = output
        self.transitions: Dict[Input, List[NonDeterministicMooreState]] = dict()
        self.distinguishers: Dict[Word, Optional[Tuple[Output, ...]]] = dict()
        self.abandoned: bool = False

    def __repr__(self) -> str:
        return f"NDMS({self.state_id} | {self.output}{' | ABANDONED' if self.abandoned else ''})"

    def identical_in_transition_outputs_and_distinguishers(
        self, o: NonDeterministicMooreState
    ) -> bool:
        if (
            not isinstance(o, type(self))
            or self.output != o.output
            or len(self.transitions) != len(o.transitions)
            or self.transitions.keys() != o.transitions.keys()
            or any(
                set(state.output for state in self.transitions[key])
                != set(state.output for state in o.transitions[key])
                for key in self.transitions
            )
        ):
            return False  # checking len of transitions.values not needed
        return self.distinguishers == o.distinguishers

    def is_fully_distinguished(self) -> bool:
        return all(word is not None for word in self.distinguishers.values())

    def shortest_path(
        self,
        condition: Callable[[NonDeterministicMooreState], bool],
        allow_abandoned: bool = False,
    ) -> Optional[List[NonDeterministicMooreState]]:
        """BFS for first state for which condition on state returns true, else None"""
        visited: Set[NonDeterministicMooreState] = set()
        queue: List[List[NonDeterministicMooreState]] = [[self]]
        while queue:
            path = queue.pop(0)
            state = path[-1]
            visited.add(state)
            if condition(state):
                return path
            for new_states in state.transitions.values():
                for new_state in new_states:
                    if new_state not in visited and (allow_abandoned or not new_state.abandoned):
                        new_path = list(path)
                        new_path.append(new_state)
                        queue.append(new_path)
        return None

    def shortest_path_to_target(
        self,
        target: NonDeterministicMooreState,
        allow_abandoned: bool = False,
    ) -> Optional[List[NonDeterministicMooreState]]:
        return self.shortest_path(lambda state: state is target, allow_abandoned=allow_abandoned)


class NonDeterministicHypothesis:
    def __init__(self, alphabet: List[Input], current_output: Output) -> None:
        self.alphabet: List[Input] = alphabet
        self.running_state_id: int = 0
        self.dist_reg: Dict[Output, Set[Word]] = dict()
        self.states: List[NonDeterministicMooreState] = []
        self.current_state = self.new_state(current_output)

    def _get_new_state_id(self) -> int:
        new_state_id = self.running_state_id
        self.running_state_id += 1
        return new_state_id

    def state_with_id(self, state_id: int) -> Optional[NonDeterministicMooreState]:
        for state in self.states:
            if state.state_id == state_id:
                return state
        return None

    def states_with_output(self, output: Output) -> List[NonDeterministicMooreState]:
        return [state for state in self.states if state.output == output]

    def new_state(self, output: Output) -> NonDeterministicMooreState:
        state = NonDeterministicMooreState(self._get_new_state_id(), output)
        if output in self.dist_reg:
            state.distinguishers = dict.fromkeys(self.dist_reg[output], None)
        self.states.append(state)
        return state

    def delete_state(self, state: NonDeterministicMooreState) -> None:
        assert state is not self.current_state
        self.states.remove(state)
        for s in self.states:
            for trans in s.transitions.values():
                if state in trans:
                    trans.remove(state)

    def merge_states(
        self,
        to_merge: NonDeterministicMooreState,
        into: NonDeterministicMooreState,
    ) -> None:
        assert to_merge in self.states
        assert into in self.states
        assert to_merge is not into
        assert to_merge.output == into.output

        # all transition to to_merge changed to into instead
        for state in self.states:
            for letter, trans in state.transitions.items():
                if to_merge in trans:
                    trans.remove(to_merge)
                    if into not in trans:
                        trans.append(into)

        # add all outgoing connections from to_merge to into
        for letter, trans in to_merge.transitions.items():
            for t in trans:
                if t not in into.transitions[letter]:
                    into.transitions[letter].append(t)

        # check that distinguishers to_merge had are also present in into
        for seq, answer in to_merge.distinguishers.items():
            if answer is not None:
                assert seq in into.distinguishers
                assert into.distinguishers[seq] == answer

        if self.current_state is to_merge:
            self.current_state = into

        self.states.remove(to_merge)

    def merge_all_states_with_same_output(self) -> None:
        # merge all states with same output
        all_outputs = set((state.output for state in self.states))
        for o in all_outputs:
            states = self.states_with_output(o)
            if len(states) > 1:
                single = states[0]
                for state in states[1:]:
                    self.merge_states(state, single)

    def is_deterministic(self) -> bool:
        return all(
            len(t) <= 1
            for state in self.states
            for t in state.transitions.values()
            if not state.abandoned
        )

    def is_oas_source(self, state: NonDeterministicMooreState) -> bool:
        if self.is_over_abstracted(state):
            return False
        for trans in state.transitions.values():
            for t in trans:
                if not t.abandoned and self.is_over_abstracted(t):
                    return True
        return False

    def is_over_abstracted(self, state: Optional[NonDeterministicMooreState] = None) -> bool:
        """Checks if a certain state - or any state if state is None - is over abstracted"""
        if state is None:
            return any(
                self.is_over_abstracted(state) for state in self.states if not state.abandoned
            )
        else:
            return not (
                state.is_fully_distinguished()
                and state.distinguishers.keys() == self.dist_reg.get(state.output, set())
            )

    def is_transition_missing(self, state: Optional[NonDeterministicMooreState] = None) -> bool:
        """Checks if a certain state - or any state if state is None - is missing a transition"""
        if state is None:
            return any(
                self.is_transition_missing(state) for state in self.states if not state.abandoned
            )
        else:
            assert not state.abandoned
            return (
                len(state.transitions) < len(self.alphabet)
                or any(len(t) == 0 for t in state.transitions.values())
                or any(all(s.abandoned for s in t) for t in state.transitions.values())
            )

    def provide_missing_transition(self, state: NonDeterministicMooreState) -> Optional[Input]:
        for letter in self.alphabet:
            if (
                letter not in state.transitions.keys()
                or len(state.transitions[letter]) == 0
                or all(s.abandoned for s in state.transitions[letter])
            ):
                return letter
        return None

    def update_distinguishers(
        self,
        state: Optional[NonDeterministicMooreState] = None,
    ) -> None:
        """Update the distinguishers of a state - or all states if state is None -
        to reflect self.distinguishers.
        Note: Only Sequences are added, answers are not removed"""
        if state is None:
            for state in self.states:
                self.update_distinguishers(state)
        else:
            if state.output in self.dist_reg:
                for dis in self.dist_reg[state.output]:
                    if dis not in state.distinguishers:
                        state.distinguishers[dis] = None

    def update_step(self, letter: Input, output: Output) -> bool:
        """Takes a step and returns True if it conforms to expected hypothesis, else False"""
        assert letter in self.alphabet and output is not None
        if letter not in self.current_state.transitions:
            self.current_state.transitions[letter] = []
        transitions = self.current_state.transitions[letter]

        possible_transitions = list(filter(lambda s: s.output == output, transitions))

        if len(possible_transitions) == 0:
            # no existing transition to state with this output
            expected = False

            # search for state we could be in now
            states = self.states_with_output(output)
            if len(states) == 0:
                # unknown output state, create a new one
                new_state = self.new_state(output)
            elif len(states) == 1:
                # there is another state (not in transition) with new_output
                # assume we entered that state instead
                new_state = states[0]
            else:
                # there are multiple states with the same output (none of which is in transition)
                # use state which is not (partially) distinguished or
                # create new state instead; makeing sure it is OAS to potentially get merged later
                new_state = states[0]  # always choose the same state (first created)

        elif len(possible_transitions) == 1:
            new_state = possible_transitions[0]
            expected = not new_state.abandoned
        else:
            assert (
                False
            ), f"there are multiple states in transition, \
                with the same output {output}: {self.current_state} with {letter} -> {transitions}"

        # update state -> new_state transition
        if new_state not in transitions:
            # has one *or more* different transitions
            transitions.append(new_state)

        # update new current_state
        self.current_state = new_state
        self.current_state.abandoned = False

        return expected

    def gen_hypothesis(
        self, starting_state: Optional[NonDeterministicMooreState] = None
    ) -> MooreMachine:
        class AbandonedWrapper:
            def __init__(self, output: Output) -> None:
                self.output = output

            def __eq__(self, __o: object) -> bool:
                return self.output == __o

            def __repr__(self) -> str:
                return str(self.output) + " | AB"

        # create all states
        states = [
            MooreState(
                nd_state.state_id,
                AbandonedWrapper(nd_state.output) if nd_state.abandoned else nd_state.output,
            )
            for nd_state in self.states
        ]

        # choose initial state
        assert starting_state is None or starting_state in self.states
        initial_state = (
            states[0]
            if starting_state is None
            else next(state for state in states if state.state_id == starting_state.state_id)
        )

        assert len(self.states) == len(states), "There should be exactly the same amount of states"
        # set all transitions
        for nd_state, state in zip(self.states, states):
            for letter, nd_next_states in nd_state.transitions.items():
                for i, nd_next_state in enumerate(nd_next_states):
                    try:
                        next_state = next(
                            next_state
                            for next_state in states
                            if next_state.state_id == nd_next_state.state_id
                        )
                    except StopIteration:
                        assert False, f"State {nd_next_state} should have existed in {states}"
                    nt = letter + (f"__{i}" if len(nd_next_states) > 1 else "")
                    state.transitions[nt] = next_state

        # create automaton and characterization set
        automaton = MooreMachine(initial_state, states)
        automaton.characterization_set = [tuple([a]) for a in self.alphabet]
        # empty word for moore
        automaton.characterization_set.insert(0, tuple())

        # calculate prefixes
        for state in states:
            state.prefix = automaton.get_shortest_path(automaton.initial_state, state)

        return automaton

    def gen_det_hypothesis(
        self, starting_state: Optional[NonDeterministicMooreState] = None
    ) -> MooreMachine:
        assert starting_state is None or starting_state in self.states
        assert starting_state is None or not starting_state.abandoned
        # abandoned_sink = MooreState(-1, "abandoned-sink")
        states = [
            MooreState(nd_state.state_id, nd_state.output)
            for nd_state in self.states
            if not nd_state.abandoned
        ]
        nd_states = [s for s in self.states if not s.abandoned]
        assert len(nd_states) == len(states)

        initial_state = (
            states[0]
            if starting_state is None
            else next(state for state in states if state.state_id == starting_state.state_id)
        )

        # set all transitions
        for nd_state, state in zip(nd_states, states):
            assert nd_state.state_id == state.state_id
            assert nd_state.output == state.output
            for letter, nd_next_states in nd_state.transitions.items():
                assert (
                    nd_next_states
                ), f"Require at least one transition for det state: {nd_state} with {letter}"

                assert not all(
                    s.abandoned for s in nd_next_states
                ), f"ALL state transitions abanoned from {nd_state} with {letter} to {nd_next_states}"
                next_nd_state = next(s for s in nd_next_states if not s.abandoned)
                assert all(
                    s.abandoned for s in nd_next_states if s is not next_nd_state
                ), "All other states SHOULD be abanoned"
                next_state = next(s for s in states if s.state_id == next_nd_state.state_id)
                state.transitions[letter] = next_state

        # create automaton and characterization set
        automaton = MooreMachine(initial_state, states)
        automaton.characterization_set = [tuple([a]) for a in self.alphabet]
        # empty word for moore
        automaton.characterization_set.insert(0, tuple())

        # calculate prefixes
        for state in states:
            state.prefix = automaton.get_shortest_path(automaton.initial_state, state)

        return automaton


class SingleTrace:
    def __init__(self) -> None:
        self.trace: List[Tuple[Output, Input, Output]] = []
        self.last_calc = 0

    def update_step(
        self,
        input_state: NonDeterministicMooreState,
        letter: Input,
        output_state: NonDeterministicMooreState,
        transition_output: Optional[Output] = None,
    ) -> None:
        # if len(self.trace) > 100000:
        #     raise RuntimeError("DEBUG: Trace was too long")
        self.trace.append((input_state.output, letter, output_state.output, transition_output))

    def _gen_correlation_difference(self) -> Set[Tuple[int, int]]:
        buckets: Dict[Tuple[int, Input], List[int]] = dict()
        for i, t in enumerate(self.trace[self.last_calc :], start=self.last_calc):
            key = t[:2]
            if key not in buckets:
                buckets[key] = []
            buckets[key].append(i)

        corr = set()
        for i, t in enumerate(self.trace):
            key = t[:2]
            for index in buckets.get(key, []):
                if t[2] != self.trace[index][2]:
                    if (index, i) not in corr:
                        corr.add((i, index))
        self.last_calc = len(self.trace)
        return corr

    def calculate_and_update_distinguishers(self, all_dis: Dict[Output, Set[Word]]) -> bool:
        def add_dis_if_not_prefix(dis: Word, out: Output) -> bool:
            changed = False
            if out in all_dis:
                for d in all_dis[out]:
                    if len(d) >= len(dis):
                        # if already existing distinguisher (d) is prefix of new distinguisher (dis)
                        # then we don't add it (the existing one already covers everything dis would)
                        if d[: len(dis)] == dis:
                            break
                    elif dis[: len(d)] == d:
                        # the new distinguisher (dis) is longer than d which is a prefix of dis
                        # in this case, dis covers everything d did, remove d and add dis
                        all_dis[out].remove(d)
                        all_dis[out].add(dis)
                        changed = True
                        break
                else:
                    all_dis[out].add(dis)
                    changed = True
            else:
                all_dis[out] = set([dis])
                changed = True
            return changed

        ac_diff = self._gen_correlation_difference()
        changed = False
        for i1, i2 in ac_diff:
            assert self.trace[i1][0] == self.trace[i2][0]
            assert self.trace[i1][1] == self.trace[i2][1]
            out, letter = self.trace[i1][0], self.trace[i1][1]

            distinguisher = [letter]

            # add identical precursors to distinguisher
            while True:
                dis = tuple(distinguisher)
                changed = add_dis_if_not_prefix(dis, out) or changed

                # Simple alternative to add_dis_if_not_prefix without optimization. Should still work
                # if out not in all_dis:
                #     all_dis[out] = set()
                # changed = changed or dis in all_dis[out]
                # all_dis[out].add(dis)

                i1 -= 1
                i2 -= 1

                if (
                    i1 < 0
                    or i2 < 0
                    or self.trace[i1][0] != self.trace[i2][0]
                    or self.trace[i1][1] != self.trace[i2][1]
                ):
                    break

                out, letter = self.trace[i1][0], self.trace[i1][1]
                distinguisher.insert(0, letter)

        return changed

    def get_word_from_trace(self, from_index: int, to_index: int) -> Tuple[Word, List[Output]]:
        """Generate word and path from index to index, both inclusive"""
        response = [entry[0] for entry in self.trace[from_index : (to_index + 1)]]
        word = tuple([entry[1] for entry in self.trace[from_index:to_index]])
        return word, response

    def generate_words(
        self,
        target: NonDeterministicMooreState,
        source: NonDeterministicMooreState,
        max_len: int = 1,
    ) -> Generator[Tuple[Word, List[Output]], None, None]:
        """Generate all words and paths from source to target that
        have a max length of max_len.
        The words are generated in order:
            * First words that have the exact state as a target
            * Then also words that lead to states with the same output as target
        Both sorted by word length and then by most recent word"""
        if max_len <= 0:
            return

        # collect trace indices
        same_output = []
        for index in reversed(range(len(self.trace))):
            entry = self.trace[index]
            if entry[2] == target.output:
                same_output.append(index)

        # tried = set()  # TODO
        for i in range(1, max_len + 1):
            for index in same_output:
                src_index = index - i
                if src_index < 0:
                    continue
                entry = self.trace[src_index]
                if entry[0] is source.output:
                    yield self.get_word_from_trace(src_index, index)


class NonDeterministicSulWrapper(SUL):
    def __init__(
        self,
        sul: SUL,
        hypothesis: NonDeterministicHypothesis,
        trace: SingleTrace,
    ) -> None:
        super().__init__()
        self.sul = sul
        self.hypothesis = hypothesis
        self.trace = trace
        self._expected: bool = True
        self._raise_on_expected: bool = False

    def add_info(self, info: Dict[str, Any]):
        """Can be used by extensions to add information at the end of the algorithm"""
        pass

    def pre(self) -> None:
        self.sul.pre()
        self._raise_on_expected = True
        self._navigate_reset()

    def post(self) -> None:
        self._raise_on_expected = False
        self.sul.post()

    def step(self, letter: Input) -> Output:
        out = self.sul.step(letter)
        if letter is not None:
            self.update_hypothesis_and_trace(letter, out)
            if self._raise_on_expected and not self._expected:
                self._raise_on_expected = False
                raise NonDeterminismFound
        return out

    def step_expected(self, letter: Input) -> bool:
        self.step(letter)
        return self._expected

    def update_hypothesis_and_trace(self, letter: Input, out: Output) -> None:
        last = self.hypothesis.current_state
        self._expected = self.hypothesis.update_step(letter, out)
        current = self.hypothesis.current_state
        self.trace.update_step(last, letter, current, out)

    def get_starting_state(self) -> NonDeterministicMooreState:
        for entry in self.trace.trace:
            c = list(
                filter(lambda s: not s.abandoned, self.hypothesis.states_with_output(entry[0]))
            )
            starting_state = c[0] if len(c) > 0 else None
            if starting_state is not None:
                break
        else:
            assert False, "All states in trace are abandoned"
        return starting_state

    def _navigate_reset(self) -> None:
        # assert not self.hypothesis.is_over_abstracted()
        assert (
            not self.hypothesis.is_transition_missing()
        ), "Transitions are missing from generated hypothesis in eq-query"
        starting_state = self.get_starting_state()
        reached = navigate_to_target_directly(self.step_expected, starting_state, self.hypothesis)
        if not reached:
            raise NonDeterminismFound


def word_from_path(path: List[NonDeterministicMooreState]) -> Word:
    assert len(path) > 0
    word = []
    for s1, s2 in zip(path, path[1:]):
        for letter, states in s1.transitions.items():
            if s2 in states:
                word.append(letter)
                break
        else:
            assert False
    return tuple(word)


def abandon_all_unreachable(hypothesis: NonDeterministicHypothesis) -> bool:
    change = False
    for state in hypothesis.states:
        if hypothesis.current_state.shortest_path_to_target(state) is None:
            state.abandoned = True
            change = True
    return change


def navigate_to_target_directly(
    step_expected_func: Callable[[Input], bool],
    target: NonDeterministicMooreState,
    hypothesis: NonDeterministicHypothesis,
    allow_abandoned: bool = False,
) -> Optional[bool]:
    """Navigate to target on shortest path in hypothesis using the step_expected_func.

    Note: It is the job of step_expected_func to actually update the hypothesis

    Returns: True if target is reached,
             False if target could not be reached,
             None if step_expected_func returned False ie. did not conform to hypothesis
    """
    assert target in hypothesis.states

    if hypothesis.current_state is target:
        return True

    path = hypothesis.current_state.shortest_path_to_target(
        target, allow_abandoned=allow_abandoned
    )
    if path is None:
        return False
    word = word_from_path(path)
    for letter, result_state in zip(word, path[1:]):
        expected = step_expected_func(letter)
        if not expected:
            return None
        if hypothesis.current_state is target:
            return True
        if hypothesis.current_state.output != result_state.output:
            # we took a different (but expected) path
            break
    return False


def navigate_to_target(
    step_expected_func: Callable[[Input], bool],
    target: NonDeterministicMooreState,
    hypothesis: NonDeterministicHypothesis,
    trace: SingleTrace,
    max_depth: int,
    allow_abandoned: bool = False,
) -> Optional[bool]:
    """Navigate to target in hypothesis using the step_expected_func and trace.
    max_depth is the max len of words extracted form trace.
    If max_depth is 0 then trace will not be used for navigation.

    Note: It is the job of step_expected_func to actually update the hypothesis and trace

    Returns: True if target is reached,
             False if target could not be reached in given max_depth,
             None if step_expected_func returned False ie. did not conform to hypothesis
    """
    assert max_depth >= 0

    sources: Dict[
        NonDeterministicMooreState,
        Generator[Tuple[Word, List[NonDeterministicMooreState]], None, None],
    ] = dict()
    while True:
        if hypothesis.current_state not in sources:
            source_state = hypothesis.current_state
            direct = navigate_to_target_directly(
                step_expected_func,
                target,
                hypothesis,
                allow_abandoned=allow_abandoned,
            )
            if direct is not False:
                # True or None
                return direct
            sources[source_state] = trace.generate_words(
                target=target,
                source=source_state,
                max_len=max_depth,
            )
            continue

        try:
            word, response = next(sources[hypothesis.current_state])
        except StopIteration:
            target.abandoned = True
            return False

        for letter, result_output in zip(word, response[1:]):
            expected = step_expected_func(letter)
            if not expected:
                return None
            if hypothesis.current_state is target:
                return True
            if hypothesis.current_state.output != result_output:
                # we took a different (but expected) path
                break


def navigate_to_nearest(
    step_expected_func: Callable[[Input], bool],
    condition: Callable[[NonDeterministicMooreState], bool],
    hypothesis: NonDeterministicHypothesis,
    trace: SingleTrace,
    max_depth: int = 0,
) -> Optional[bool]:

    path = hypothesis.current_state.shortest_path(condition)
    if path is None:
        return False

    target = path[-1]
    assert not target.abandoned
    reached = navigate_to_target(step_expected_func, target, hypothesis, trace, max_depth)
    return reached


def explore_missing(
    step_expected_func: Callable[[Input], bool],
    hypothesis: NonDeterministicHypothesis,
    trace: SingleTrace,
    max_depth: int,
) -> bool:
    """Explore missing transitions until the hypothesis is input complete
    or abandon states that cannot be reached anymore.

    Note: It is the job of step_expected_func to actually update the hypothesis and trace"""
    assert max_depth >= 0

    def explore_single_missing(target: NonDeterministicMooreState) -> None:
        assert not target.abandoned
        reached = navigate_to_target(
            step_expected_func,
            target,
            hypothesis,
            trace,
            max_depth,
        )
        if reached is True:
            letter = hypothesis.provide_missing_transition(hypothesis.current_state)
            assert letter is not None
            step_expected_func(letter)

    changed = False
    path = hypothesis.current_state.shortest_path(hypothesis.is_transition_missing)
    while path is not None:
        changed = True
        explore_single_missing(path[-1])
        path = hypothesis.current_state.shortest_path(hypothesis.is_transition_missing)
    return changed


def mark_over_abstracted(hypothesis: NonDeterministicHypothesis, trace: SingleTrace) -> bool:
    return trace.calculate_and_update_distinguishers(hypothesis.dist_reg)


def distinguish_over_abstracted(ndsul: NonDeterministicSulWrapper, max_word_len: int) -> bool:
    class UnexpectedBehaviour(Exception):
        pass

    class DudTrace:
        def update_step(*args, **kwargs):
            pass

    h = copy.deepcopy(ndsul.hypothesis)
    t = ndsul.trace
    sul = NonDeterministicSulWrapper(ndsul, h, DudTrace())
    # use sul instead of ndsul directly while working on new h

    def step_expected_raise(letter: Input) -> bool:
        expected = sul.step_expected(letter)
        if not expected:
            raise UnexpectedBehaviour
        return expected

    def step_expected_raise_not_in(state: NonDeterministicMooreState, letter: Input) -> bool:
        allowed = h.current_state is state
        expected = sul.step_expected(letter)
        if not expected and not allowed:
            raise UnexpectedBehaviour
        return expected

    def navigate_to_oas_source() -> NonDeterministicMooreState:
        reached = navigate_to_nearest(
            step_expected_raise,
            h.is_oas_source,
            h,
            t,
            max_depth=max_word_len,
        )
        if not reached:
            raise UnexpectedBehaviour
        assert not h.is_over_abstracted(h.current_state), "Oas source must not be over abstracted"
        return h.current_state

    def apply_transition() -> Input:
        path = h.current_state.shortest_path(h.is_over_abstracted)
        assert path is not None
        assert len(path) == 2
        word = word_from_path(path)
        assert len(word) == 1
        step_expected_raise(word[0])
        if h.current_state != path[1]:
            raise UnexpectedBehaviour
        assert h.is_over_abstracted(h.current_state), "Must be in OAS after applying transition"
        return word[0]

    def find_empty_distinguisher() -> Word:
        for dis in sorted(oas_state.distinguishers.keys()):
            outputs = oas_state.distinguishers[dis]
            if outputs is None:
                return dis
        else:
            assert False, "Had fully distinguished OAS state still (should have been deleted)"

    def apply_distinguisher(dis: Word) -> List[NonDeterministicMooreState]:
        assert dis is not None and len(dis) > 0
        visited: List[NonDeterministicMooreState] = []
        for letter in dis:
            step_expected_raise(letter)
            visited.append(h.current_state)
        return visited

    def split_state_from(
        oas_state: NonDeterministicMooreState,
        dis: Word,
        visited: List[NonDeterministicMooreState],
        transition_letter: Input,
        oas_source: NonDeterministicMooreState,
    ) -> NonDeterministicMooreState:
        new_state = h.new_state(oas_state.output)
        new_state.distinguishers = copy.copy(oas_state.distinguishers)
        new_state.distinguishers[dis] = tuple(state.output for state in visited)
        new_state.transitions = {key: [] for key in h.alphabet}
        new_state.transitions[dis[0]] = [visited[0]]

        source_t = oas_source.transitions[transition_letter]
        source_t.remove(oas_state)
        source_t.append(new_state)

        return new_state

    def potentially_merge_states(
        new_state: NonDeterministicMooreState,
        transition_letter: Input,
        oas_source: NonDeterministicMooreState,
    ) -> NonDeterministicMooreState:
        for state in h.states_with_output(new_state.output):
            if (
                state is not new_state
                and state.identical_in_transition_outputs_and_distinguishers(new_state)
            ):
                h.states.remove(new_state)
                source_t = oas_source.transitions[transition_letter]
                source_t.remove(new_state)
                source_t.append(state)
                return state
        return new_state

    def navigate_to_non_oas() -> None:
        if h.is_over_abstracted(h.current_state):
            reached = navigate_to_nearest(
                step_expected_raise,
                lambda state: not h.is_over_abstracted(state),
                h,
                t,
                max_depth=max_word_len,
            )
            if not reached:
                raise UnexpectedBehaviour
            assert not h.is_over_abstracted(h.current_state)

    def delete_allowed_oas() -> None:
        assert not h.is_over_abstracted(h.current_state)
        delete_states = [
            state
            for state in filter(h.is_over_abstracted, h.states)
            if not state.abandoned
            if h.current_state.shortest_path_to_target(state) is None
        ]

        for state in delete_states:
            h.delete_state(state)

        if h.is_transition_missing():
            raise UnexpectedBehaviour

    # clear all distinguishers on states
    for state in h.states:
        state.distinguishers = {}
    # merge all states where multiple states with same output exist
    h.merge_all_states_with_same_output()
    h.update_distinguishers()
    i = 0
    try:
        while h.is_over_abstracted():
            i = i + 1
            if all(h.is_over_abstracted(state) for state in h.states if not state.abandoned):
                raise RuntimeError("Known limitation: all states are over abstracted")

            # navigation
            oas_source = navigate_to_oas_source()
            transition_letter = apply_transition()
            oas_state = h.current_state
            dis = find_empty_distinguisher()
            visited = apply_distinguisher(dis)

            new_state = split_state_from(oas_state, dis, visited, transition_letter, oas_source)

            explore_missing(partial(step_expected_raise_not_in, new_state), h, t, max_word_len)
            if new_state.abandoned:
                raise UnexpectedBehaviour

            # debug output, get partial hypothesis
            # save_automaton_to_file(
            #     h.gen_hypothesis(), path=f"easy_ex_dis_{i}.dot", file_type="dot"
            # )
            # save_automaton_to_file(
            #     h.gen_hypothesis(), path=f"easy_ex_dis_{i}.pdf", file_type="pdf"
            # )

            new_state = potentially_merge_states(new_state, transition_letter, oas_source)
            navigate_to_non_oas()
            delete_allowed_oas()
    except UnexpectedBehaviour:
        return False

    assert not h.is_over_abstracted()
    assert (
        h.is_deterministic()
    ), "Hypothesis should be deterministic but is not after distinguishing"
    ndsul.hypothesis = h
    return True


def run_wmoore(
    alphabet: List[Input],
    sul: SUL,
    eq_oracle_generator: Callable[[SUL], Oracle],
    max_word_len: int,
    return_data: bool = False,
    visualize_with_name: Optional[str] = None,
    print_level: Literal[0, 1, 2, 3] = 2,
) -> Union[Automaton, Tuple[Automaton, Dict[str, Any]]]:
    """Executes w-moore algorithm.

    Args:

        alphabet: input alphabet

        sul: system under learning

        eq_oracle_generator: function which generates the desired equivalence oracle
                             with only the SUL as an argument

        return_data: if True, a map containing all information (runtime/#queries/#steps)
                     will be returned (Default value = False)

        visualize_with_name: if None no visualization will occur,
                             else visualizes all deterministic hypotheses with given name

        print_level: 0 - None,
                     1 - just results,
                     2 - current round and hypothesis size,
                     3 - educational/debug
                     (Default value = 2)

    Returns:

        learned automaton (dict containing all information about learning if 'return_data' is True)

    """
    start_time = time.time()
    eq_query_time = 0.0
    learning_rounds = 0
    hypothesis: Optional[Automaton] = None

    ndsul = NonDeterministicSulWrapper(
        sul,
        NonDeterministicHypothesis(alphabet, sul.step(None)),
        SingleTrace(),
    )

    eq_oracle = eq_oracle_generator(ndsul)

    def visualize_hypothesis(iteration: str) -> None:
        if visualize_with_name is not None:
            vis_path = f"{visualize_path}{visualize_with_name}"
            save_automaton_to_file(hypothesis, path=f"{vis_path}_{iteration}", file_type="dot")
            save_automaton_to_file(hypothesis, path=f"{vis_path}_{iteration}", file_type="pdf")

    def debug_print_states() -> None:
        if print_level > 2:
            for state in ndsul.hypothesis.states:
                if ndsul.hypothesis.is_over_abstracted(state):
                    print(f"D: {state} OAS with dis:", end=" ")
                    # print(state.distinguishers)
                    print(ndsul.hypothesis.dist_reg[state.output])
                    # print(f"D: {state.transitions.items()}")
                else:
                    print(f"D: {state}")
            print()

    def dprint(msg: str) -> None:
        if print_level > 2:
            print(msg)

    def log() -> None:
        import csv

        if visualize_with_name is not None:
            path = f"{visualize_path}{visualize_with_name}_round_{learning_rounds}"
            with open(f"{path}.csv", "w", newline="") as csvfile:
                writer = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
                writer.writerow(["State Transition Table:"] + ndsul.hypothesis.alphabet)
                for state in ndsul.hypothesis.states:
                    result = [str(state)]
                    for letter in ndsul.hypothesis.alphabet:
                        if letter in state.transitions:
                            result.append(" | ".join(str(s) for s in state.transitions[letter]))
                        else:
                            result.append(str(None))
                    writer.writerow(result)
                writer.writerow([])
                writer.writerow(["Distinguishers:"])
                for key, dis in ndsul.hypothesis.dist_reg.items():
                    writer.writerow([key] + sorted(dis))
                writer.writerow([])
                writer.writerow(["Trace:"])
                for i, entry in enumerate(ndsul.trace.trace):
                    writer.writerow(it.chain([i], entry))
        return None

    try:
        changed, lastChanged = True, True
        while changed or lastChanged:
            lastChanged = changed
            learning_rounds += 1
            if print_level > 1:
                print(f"Starting learning round {learning_rounds}")
                print(f"Len of trace is {len(ndsul.trace.trace)}")
                print(f"Number of states {len(ndsul.hypothesis.states)}")

            assert learning_rounds < 100, "DEBUG: Learning round limit reached"

            dprint("I: 1A: Explore missing")
            changed = explore_missing(
                ndsul.step_expected, ndsul.hypothesis, ndsul.trace, max_word_len
            )
            dprint(f"I: explore missing changed: {changed}")
            changedUnreachable = abandon_all_unreachable(ndsul.hypothesis)
            dprint(f"I: abandon all unreachable changed: {changedUnreachable}")
            # changed = changedUnreachable or changed # TODO: test like this

            if after_1a_hook is not None:
                after_1a_hook(ndsul)

            # log()
            # hypothesis = ndsul.hypothesis.gen_hypothesis(ndsul.get_starting_state())
            # visualize_hypothesis(f"round_{learning_rounds}")
            # dprint(
            #     f"Len trace: {len(ndsul.trace.trace)}, with last entry: {ndsul.trace.trace[-1]}"
            # )

            if ndsul.hypothesis.is_deterministic():
                dprint("I: Start searching for counter-example")
                debug_print_states()
                assert (
                    not ndsul.hypothesis.is_transition_missing()
                ), "Missing transitions before eq-query altough explore missing terminated"
                hypothesis = ndsul.hypothesis.gen_det_hypothesis(ndsul.get_starting_state())
                eq_query_start = time.time()
                try:
                    cex = eq_oracle.find_cex(hypothesis)
                    if cex is None:
                        break
                    print(f"E: Found counterexample during eq-query: {cex}")
                    assert False, "Unreachable. Expected an exception while finding cex"
                except NonDeterminismFound:
                    dprint("W: Could not navigate to starting state in eq-query or found cex")
                finally:
                    eq_query_time += time.time() - eq_query_start
                    changed = True
                continue

            dprint("I: 1B: Marking over abstracted")
            changedMarked = mark_over_abstracted(ndsul.hypothesis, ndsul.trace)
            dprint(f"Mark OAS changed: {changedMarked}")
            changed = changedMarked or changed
            debug_print_states()

            assert (
                ndsul.hypothesis.is_over_abstracted()
            ), "Non-determism of hypothesis should imply over abstracted"
            dprint("I: 1C: Distinguishing over abstracted")
            changeDis = distinguish_over_abstracted(ndsul, max_word_len)
            dprint(f"I: Successfully updated h: {changeDis}")
            changed = changeDis or changed
        else:
            raise RuntimeError(f"No change detected after {learning_rounds} rounds")
    except Exception as e:
        # log()
        # hypothesis = ndsul.hypothesis.gen_hypothesis(ndsul.get_starting_state())
        # visualize_hypothesis("fail")
        raise

    log()
    visualize_hypothesis("success")
    with open("trace.log", "w") as f:
        f.write(str(list(enumerate(ndsul.trace.trace))))

    # end algorithm

    total_time = round(time.time() - start_time, 2)
    eq_query_time = round(eq_query_time, 2)
    learning_time = round(total_time - eq_query_time, 2)

    info = {
        "learning_rounds": learning_rounds,
        "automaton_size": len(hypothesis.states),
        "queries_learning": sul.num_queries,
        "steps_learning": sul.num_steps,
        "queries_eq_oracle": eq_oracle.num_queries,
        "steps_eq_oracle": eq_oracle.num_steps,
        "learning_time": learning_time,
        "eq_oracle_time": eq_query_time,
        "total_time": total_time,
        "characterization set": hypothesis.characterization_set,
        "max_word_len": max_word_len,
    }

    if print_level > 0:
        from pprint import pprint

        pprint(info)

    ndsul.add_info(info)
    if return_data:
        return hypothesis, info

    return hypothesis


def learn_with_wmoore(
    moore_machine: MooreMachine,
    max_word_len: int,
    visualize_with_name: Optional[str] = None,
    visualize_only_final: bool = False,
    print_level: int = 3,
) -> Automaton:
    if visualize_with_name is not None:
        vis_path = f"{visualize_path}{visualize_with_name}"
        visualize_automaton(moore_machine, path=f"{vis_path}_original")

    alphabet = sorted(moore_machine.get_input_alphabet())

    non_resetting_sul = NonResettingMooreSUL(moore_machine)

    learned_moore = run_wmoore(
        alphabet,
        non_resetting_sul,
        partial(PerfectEqOracle, alphabet),
        max_word_len,
        return_data=False,
        visualize_with_name=None if visualize_only_final else visualize_with_name,
        print_level=print_level,
    )

    if visualize_with_name is not None:
        visualize_automaton(learned_moore, path=f"{vis_path}_final")

    assert not isinstance(learned_moore, tuple)
    assert len(learned_moore.states) <= len(moore_machine.states), "Hypothesis has to many states!"

    return learned_moore


def main() -> int:
    from tqdm import tqdm

    import examples as exm

    # test examples with base algorithm
    num_examples = 13
    for i in tqdm(range(1, num_examples + 1)):
        generator = exm.__dict__[f"gen_example_{i}"]
        machine = generator()
        # print(f"Learning Example {i}")
        exm.run_comparison_and_check(machine, 10)

    # test time extension (test last as loading global extension patches classes)
    exm.test_time_extension(exm.gen_sm_1(), "WAIT")


if __name__ == "__main__":
    exit(main())
