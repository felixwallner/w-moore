from collections import Counter
from typing import List

from aalpy.automata import MooreMachine
from aalpy.base.Automaton import Automaton


def get_alphabet(machine: Automaton) -> List[str]:
    """Get alphabet from any machine, no matter if it is input complete or not"""
    return sorted({key for state in machine.states for key in state.transitions.keys()})


def count_outputs(machine: Automaton) -> Counter:
    counter = Counter([state.output for state in machine.states])
    assert sum(counter.values()) == len(machine.states)
    return counter


def unique_outputs(machine: Automaton) -> int:
    """The number of outputs occuring only once (= on a single states)"""
    return sum(value == 1 for value in count_outputs(machine).values())


def angelic_completion(moore_machine: MooreMachine) -> None:
    """Make the machine input complete such that all missing transitions are self-transitions"""
    if not moore_machine.is_input_complete():
        print("W: Machine is not input complete. Using self transitions...")
        alphabet = get_alphabet(moore_machine)
        for state in moore_machine.states:
            for letter in alphabet:
                if letter not in state.transitions:
                    state.transitions[letter] = state
